import { configureStore } from "@reduxjs/toolkit";
import counterReducer from '../features/counter/counterSlice'

export const store: object= configureStore({
    reducer: {
            counter:counterReducer,
    }
})