import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { increment, decrement, reset, incrementByAmount } from './counterSlice'
import {useState} from 'react'

function Counter() {
  const count = useSelector((state)=> state.counter.count)
  const dispatch= useDispatch()
  const [amount,setAmount] = useState(0)
  const addValue= Number(amount) || 0

  const resetAll = () =>{
    setAmount(0)
    dispatch(reset())
  
  }

  return(
    <section>
      <p>{count}</p>
      <div>
        <button onClick={()=> dispatch(increment())}>+</button>
        <button onClick={()=> dispatch(decrement())}>-</button><br></br>
       
        <input onChange={(e)=>setAmount(Number(e.target.value))}/>

        <div>
          <button onClick={()=> dispatch(incrementByAmount(addValue))}>Add Amount</button>
        </div>
        <div>
          <button onClick={resetAll}>Reset</button>
        </div>
      
      </div>
    </section>
  )
}


export default Counter
